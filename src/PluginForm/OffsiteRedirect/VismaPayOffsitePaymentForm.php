<?php

namespace Drupal\commerce_payment_vismapay\PluginForm\OffsiteRedirect;

use function GuzzleHttp\json_encode;
use CommerceGuys\Addressing\Country\CountryRepository;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;

/**
 * Provides Visma Pay API PaymentForm class for off-site payments.
 */
class VismaPayOffsitePaymentForm extends BasePaymentOffsiteForm {

  /**
   * The Charge redirection URL.
   *
   * @var string
   *
   * @NOTE {:token} MUST be added at the end of URL.
   */
  protected $urlRedirectionToPayment = 'https://www.vismapay.com/pbwapi/token/';

  /**
   * The Token Request URL.
   *
   * @var string
   */
  protected $urlTokenRequest = 'https://www.vismapay.com/pbwapi/auth_payment';

  /**
   * The Token.
   *
   * @var string
   */
  protected $paymentToken;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $configuration = $payment_gateway_plugin->getConfiguration();

    if (!$order->getTotalPrice()->isPositive()) {
      throw new \Exception('Order ID "' . $payment->getOrderId() . '": Total price is not positive amount. Visma Pay API does not accept zero or negative amounts.');
    }

    // Payment gateway configuration data.
    $data['version'] = $configuration['api_version'];
    $data['api_key'] = $payment_gateway_plugin->getFinalApiKey();

    // Amount is in fractional monetary units e.g. 1€ = 100.
    $data['amount'] = $order->getTotalPrice()->getNumber() * 100;
    $data['currency'] = $order->getTotalPrice()->getCurrencyCode();
    $email = $order->getEmail();
    if (\Drupal::service('email.validator')->isValid($email)) {
      $data['email'] = $email;
    }

    $data['payment_method'] = $this->getPaymentObject($form);

    $order_number = $this->getOrderIdStringForApi();
    $data['order_number'] = $order_number;
    $data['authcode'] = $this->getAuthcode($order_number);
    $data['customer'] = $this->getPaymentCustomerObject();
    $data['products'] = $this->getProductsArray();
    $data['initiator'] = new \stdClass();

    $order->setData('vismapay_api_object', $data);

    try {
      $this->requestPaymentToken();
    }
    catch (\Exception $e) {
      \Drupal::logger('commerce_payment_vismapay')
        ->warning('[Visma Pay API] Payment request failed. Order id is @order_id. Message from API: :msg', [
          '@order_id' => $order->id(),
          ':msg' => $e->getMessage(),
        ]);
    }
    // Order needs saving after $data and paymentToken have been stored with
    // setData().
    $order->save();

    if (empty($this->paymentToken)) {
      \Drupal::logger('commerce_payment_vismapay')
        ->warning('[Visma Pay API] No payment token available. Order id is @order_id.', [
          '@order_id' => $order->id(),
        ]);

      \Drupal::messenger()
        ->addWarning($this->t('We are experiencing issues with the Payment Gateway. Please try again.'));

      /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
      $checkout_flow = $order->get('checkout_flow')->entity;
      $checkout_flow_plugin = $checkout_flow->getPlugin();
      $redirect_step_id = $checkout_flow_plugin->getPreviousStepId('payment');
      // This redirects user back to the review -step.
      $checkout_flow_plugin->redirectToStep($redirect_step_id);
    }

    return $this->buildRedirectForm($form, $form_state,
      $this->buildPaymentRedirectUrl(),
      []);
  }

  /**
   * Build a payment menthod -object for the payment.
   *
   * @param array $form
   *   The form structure.
   *
   * @see https://payform.bambora.com/docs/web_payments/?page=full-api-reference#payment-method
   */
  private function getPaymentObject(array $form) {

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $configuration = $payment_gateway_plugin->getConfiguration();
    $token_valid = \Drupal::time()->getRequestTime() +
      $configuration['token_validity'];
    $language = $payment_gateway_plugin->negotiateValidLanguageCode($order);
    /** @var \stdClass $paymentMethodObject */
    $paymentMethodObject = new \stdClass();
    $paymentMethodObject->type = $payment_gateway_plugin->getPaymentType();

    // Form url values.
    // Visma Pay API has no 'cancel_url' property.
    $paymentMethodObject->return_url = $this->buildReturnUrl()->toString();

    // Since we are only using e-payments, the payment is always
    // settled or cancelled upon return. Therefore no 'notify_url' is needed,
    // however it is required field in the API.
    $paymentMethodObject->notify_url = $payment_gateway_plugin->getNotifyUrl()
      ->toString();

    $paymentMethodObject->token_valid_until = $token_valid;
    $paymentMethodObject->lang = $language;

    // Supporting either all banks, or selected banks. No other payments
    // available for now.
    $paymentMethodObject->selected = $configuration['all_banks'] ? ['banks'] : array_values($configuration['selected_banks']);

    return $paymentMethodObject;
  }

  /**
   * Calculate Authcode for the Token Request.
   *
   * Https://payform.bambora.com/docs/web_payments/?page=full-api-reference#payment-token-request.
   *
   * @param string $order_number
   *   Order id to use with the payment token request.
   *
   * @return string
   *   The authentication code.
   */
  protected function getAuthcode($order_number) {

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $api_key = $payment_gateway_plugin->getFinalApiKey();
    $private_key = $payment_gateway_plugin->getFinalPrivateKey();
    $key = $api_key . '|' . $order_number;

    return strtoupper(hash_hmac('sha256', $key, $private_key));
  }

  /**
   * Get Payment customer -object.
   *
   * @return object
   *   The Payment Customer object.
   *
   * @see https://payform.bambora.com/docs/web_payments/?page=full-api-reference#payment-customer-object
   */
  protected function getPaymentCustomerObject() {

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $billing_address = $order->getBillingProfile()->get('address')
      ->first();
    /** @var \stdClass $paymentCustomerObject */
    $paymentCustomerObject = new \stdClass();
    $paymentCustomerObject->firstname = $billing_address->getGivenName();
    $paymentCustomerObject->lastname = $billing_address->getFamilyName();
    $email = $order->getEmail();
    if (\Drupal::service('email.validator')->isValid($email)) {
      $paymentCustomerObject->email = $email;
    }
    $paymentCustomerObject->address_street = $billing_address->getAddressLine1();
    if (!empty($billing_address->getAddressLine2())) {
      $paymentCustomerObject->address_street .= ', ' . $billing_address->getAddressLine2();
    }
    $paymentCustomerObject->address_city = $billing_address->getLocality();
    $paymentCustomerObject->address_zip = $billing_address->getPostalCode();
    if (!empty($billing_address->getCountryCode())) {
      $langcode = $order->language()->getId();
      if ($langcode == Language::LANGCODE_NOT_SPECIFIED) {
        $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
      }
      $country_repository = new CountryRepository($langcode);
      $country = $country_repository->get($billing_address->getCountryCode());
      $paymentCustomerObject->address_country = $country->getName();
    }

    return $paymentCustomerObject;
  }

  /**
   * Get array of products for Visma Pay Token Request.
   *
   * @return \stdClass[]
   *   Array of \stdClass objects.
   *
   * @see https://payform.bambora.com/docs/web_payments/?page=full-api-reference#item-details
   */
  protected function getProductsArray() {

    $products = [];

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    foreach ($order->getItems() as $item) {
      /** @var \stdClass $productObject */
      $productObject = new \stdClass();
      $productObject->id = $item->getPurchasedEntity()->getSku();
      $productObject->title = $item->getTitle();

      // Visma Pay API can't handle count formatted as float. However we'll
      // **NOT** prevent user's to access the payment gateway since Visma Pay
      // receives also order item prices, thus rounding is here simply to
      // satisfy the validity of API data restrictions.
      if ((int) $item->getQuantity() != $item->getQuantity()) {
        \Drupal::logger('commerce_payment_vismapay')
          ->warning('Visma Pay API (order payment) used with a rounded product quantity. Order :order_id, order item :order_item, variation sku :sku, quantity change :qty => :qty_rounded.', [
            ':order_id' => $order->id(),
            ':order_item' => $item->id(),
            ':sku' => $item->getPurchasedEntity()->getSku(),
            ':qty' => $item->getQuantity(),
            ':qty_rounded' => (int) $item->getQuantity(),
          ]);
      }
      $productObject->count = (int) $item->getQuantity();

      /* Assume 0% tax at this point and adjust if adjustments are found. */
      // Product's pretax price (price without tax). Must be in fractional
      // monetary units e.g. 1€ = 100. Can also be negative if type is 4
      // (discount).
      $productObject->pretax_price = $item->getTotalPrice()->getNumber() * 100;
      $productObject->price = $item->getTotalPrice()->getNumber() * 100;
      $productObject->tax = 0;

      /** @var \Drupal\commerce_order\Adjustment $adjustment */
      foreach ($item->getAdjustments() as $adjustment) {
        // Fingers crossed there is only one tax adjustment per $item.
        if ($adjustment->getType() == 'tax') {
          // Product's tax. Given as integer without %-sign e.g 24% = 24.
          $productObject->tax = $adjustment->getPercentage() * 100;
          $number = $adjustment->getAmount()->getNumber() * 100;
          if ($adjustment->isIncluded()) {
            $productObject->pretax_price -= $number;
          }
          else {
            $productObject->price += $number;
          }
        }
      }
      // Product's type. Used to identify different rows.
      // Value 1 indicates normal product row.
      // @see https://payform.bambora.com/docs/web_payments/?page=full-api-reference#item-details
      $productObject->type = 1;
      $products[] = $productObject;
    }

    return $products;
  }

  /**
   * Request the Payment Token for the payment redirect.
   */
  protected function requestPaymentToken() {

    $this->paymentToken = '';

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $data = $order->getData('vismapay_api_object');
    $client = \Drupal::httpClient();

    try {
      $response = $client->request('POST', $this->urlTokenRequest, [
        'json' => $data,
      ]);
      $token = json_decode($response->getBody());

      if ($response->getStatusCode() != 200 || $token->result != 0 ||
          empty($token->token)) {
        $errors = '';
        if (!empty($token->errors)) {
          $errors = implode(' ', $token->errors);
        }
        switch ($token->result) {
          case 1:
            throw new \Exception('[Visma Pay API] Error code 1: Validation error. API msg: "' . $errors . '"."');

          case 2:
            throw new \Exception('[Visma Pay API] Error code 2: Duplicate order_number. API msg: "' . $errors . '"."');

          case 10:
            throw new \Exception('[Visma Pay API] Error code 10: Maintenance break. API msg: ' . $errors . '"."');

          default:
            $encoded = json_encode($token);
            $errors .= ' Response object: "' . $encoded . '"';
            throw new \Exception('[Visma Pay API] Error - no code, perhaps no token either. API msg: "' . $errors . '"."');
        }
      }
      else {
        $this->paymentToken = $token->token;
        $order->setData('vismapay_payment_api_token', $this->paymentToken);
      }
    }
    catch (RequestException $e) {
      \Drupal::logger('commerce_payment_vismapay')
        ->warning('Visma Pay API request failed (' .
          gettype($e) . ') Order id is :order_id. Message: :message', [
            ':message' => $e->getMessage(),
            ':order_id' => $order->id(),
          ]);
    }
    catch (\Exception $e) {
      \Drupal::logger('commerce_payment_vismapay')
        ->warning('Visma Pay API returned an error. Order id is @order_id. Message: @message', [
          '@message' => $e->getMessage(),
          '@order_id' => $order->id(),
        ]);
    }
  }

  /**
   * Build the Payment redirect URL.
   */
  protected function buildPaymentRedirectUrl() {
    if (empty($this->paymentToken)) {
      return FALSE;
    }

    return $this->urlRedirectionToPayment . $this->paymentToken;
  }

  /**
   * Builds the URL to the "verifyPaymentPage" -page.
   *
   * @return \Drupal\Core\Url
   *   The "return" page URL.
   */
  protected function buildReturnUrl() {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    return Url::fromRoute('commerce_payment_vismapay.checkout.verify-payment', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

  /**
   * Build unique order nubmer for each try-out of payment gateway.
   *
   * This must be used in order to not get "payment has been processed" block
   * when entering Visma Pay API the 2nd time for example after user cancelled
   * the payment upon logging in to the e-bank. API description of the
   * field:
   * "The order number shall be unique for every payment and contain only
   * numbers, letters (a-z), dashes and underscores."
   *
   * @return string
   *   Order number. Pattern: [drupal-order-id]_[unix-timestamp]
   */
  protected function getOrderIdStringForApi() {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    // Current time msut be used since user may return from the payment
    // gateway after cancel, and retry the payment without modifying the
    // order at all - changed timestamp is intact, but the API order number
    // must be different on every payment token request.
    return $order->id() . '--' . \Drupal::time()->getRequestTime();
  }

}
